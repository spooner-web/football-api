<?php
declare(strict_types=1);

namespace SpoonerWeb\Football;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use JsonException;

class Api
{
    public static function get(string $endpoint, bool $addTimezone = false): array
    {
        if ($addTimezone) {
            $endpoint .= '/?timezone=' . Configuration::getTimezone();
        }
        $client = new Client();
        $request = new Request(
            'GET',
            Configuration::$entrypoint . $endpoint,
            [
                'X-RapidAPI-Key' => Configuration::getToken()
            ]
        );
        try {
            $response = $client->send($request);
            try {
                $result = json_decode(
                    (string)$response->getBody(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            } catch (JsonException $e) {
            }
        } catch (GuzzleException $e) {
        }

        return $result['api'] ?? [];
    }
}
