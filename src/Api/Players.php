<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Players
{
    protected static string $mainEndpoint = 'players/';

    public static function getAllAvailableSeasonsForPlayerStatistics(): array
    {
        $endpoint = self::$mainEndpoint . 'seasons/';

        return Api::get($endpoint);
    }

    public static function findAllPlayersByLastName(string $lastName): array
    {
        $endpoint = self::$mainEndpoint . 'search/' . strtolower($lastName);

        return Api::get($endpoint);
    }

    public static function getSquadByTeamAndSeason(int $teamId, int $season): array
    {
        $endpoint = self::$mainEndpoint . 'squad/' . $teamId . '/' . $season;

        return Api::get($endpoint);
    }

    public static function getAllStatisticsByPlayer(int $playerId): array
    {
        $endpoint = self::$mainEndpoint . 'player/' . $playerId;

        return Api::get($endpoint);
    }

    public static function getAllStatisticsByTeamAndSeason(int $teamId, int $season): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId . '/' . $season;

        return Api::get($endpoint);
    }

    public static function getAllStatisticsByPlayerAndSeason(int $playerId, int $season): array
    {
        $endpoint = self::$mainEndpoint . 'player/' . $playerId . '/' . $season;

        return Api::get($endpoint);
    }

    public static function getAllStatisticsByFixtureId(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . 'fixture/' . $fixtureId;

        return Api::get($endpoint);
    }

}
