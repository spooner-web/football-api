<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;
use SpoonerWeb\Football\Configuration;

class Fixtures
{
    protected static string $mainEndpoint = 'fixtures/';

    public static function findAllRoundsByLeagueId(int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . 'rounds/' . $leagueId;

        return Api::get($endpoint);
    }

    public static function findCurrentRoundByLeagueId(int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . 'rounds/' . $leagueId . '/current';

        return Api::get($endpoint);
    }

    public static function findOneFixtureById(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . 'id/' . $fixtureId;

        return array_pop(Api::get($endpoint, true)['fixtures']);
    }

    public static function findAllLive(): array
    {
        $endpoint = self::$mainEndpoint . 'live';

        return Api::get($endpoint);
    }

    public static function findLiveByLeagueIds(array $leagueIds): array
    {
        $endpoint = self::$mainEndpoint . 'live/' . implode('-', $leagueIds);

        return Api::get($endpoint, true);
    }

    public static function findAllByDate(\DateTime $dateTime): array
    {
        $endpoint = self::$mainEndpoint . 'date/' . $dateTime->format('Y-m-d');

        return Api::get($endpoint, true);
    }

    public static function findAllByLeagueId(int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId;

        return Api::get($endpoint, true);
    }

    public static function findAllByLeagueIdAndDate(int $leagueId, \DateTime $dateTime = null): array
    {
        if ($dateTime === null) {
            $dateTime = new \DateTime('now');
        }
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId . '/' . $dateTime->format('Y-m-d');

        return Api::get($endpoint, true);
    }

    public static function findAllByLeagueIdAndRound(int $leagueId, string $round = null): array
    {
        if ($round === null) {
            $round = array_shift(self::findCurrentRoundByLeagueId($leagueId)['fixtures']);
        }
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId . '/' . $round;

        return Api::get($endpoint, true);
    }

    public static function findNextByLeagueId(int $leagueId, int $next): array
    {
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId . '/next/' . $next;

        return Api::get($endpoint, true);
    }

    public static function findLastByLeagueId(int $leagueId, int $last): array
    {
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId . '/last/' . $last;

        return Api::get($endpoint, true);
    }

    public static function findAllByTeamId(int $teamId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId;

        return Api::get($endpoint, true);
    }

    public static function findAllByTeamIdAndLeagueId(int $teamId, int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId . '/' . $leagueId;

        return Api::get($endpoint, true);
    }

    public static function findNextByTeamId(int $teamId, int $next): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId . '/next/' . $next;

        return Api::get($endpoint, true);
    }

    public static function findLastByTeamId(int $teamId, int $last): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId . '/last/' . $last;

        return Api::get($endpoint, true);
    }

    public static function findAllH2hBetweenTwoLeagueIds(int $teamId1, int $teamId2): array
    {
        $endpoint = self::$mainEndpoint . 'h2h/' . $teamId1 . '/' . $teamId2;

        return Api::get($endpoint, true);
    }
}
