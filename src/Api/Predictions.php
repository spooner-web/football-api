<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Predictions
{
    protected static string $mainEndpoint = 'predictions/';

    public static function getPredictionsOfFixture(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . $fixtureId;

        return Api::get($endpoint);
    }
}
