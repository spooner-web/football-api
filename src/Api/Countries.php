<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Countries
{
    public static function all(): array
    {
        $endpoint = 'countries';

        return Api::get($endpoint);
    }
}
