<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Leagues
{
    protected static string $mainEndpoint = 'leagues/';

    public static function all(): array
    {
        $endpoint = self::$mainEndpoint;

        return Api::get($endpoint);
    }

    public static function findOneById(int $leagueId, bool $getLeagueDirectly = false): array
    {
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId;

        if ($getLeagueDirectly) {
            $result = Api::get($endpoint)['leagues'][0];
        } else {
            $result = Api::get($endpoint);
        }
        return $result;
    }

    public static function findAllByTeamId(int $teamId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId;

        return Api::get($endpoint);
    }

    public static function findAllByTeamIdAndSeason(int $teamId, int $seasonAsYear): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId . '/' . $seasonAsYear;

        return Api::get($endpoint);
    }

    public static function findAllByTeamIdAndCurrentSeason(int $teamId): array
    {
        return self::findAllByTeamIdAndSeason($teamId, Seasons::currentByTeam($teamId));
    }

    public static function findAllBySearchString(string $search): array
    {
        $endpoint = self::$mainEndpoint . 'search/' . strtolower(str_replace(' ', '_', $search));

        return Api::get($endpoint);
    }

    public static function findAllByCountry(string $country): array
    {
        $endpoint = self::$mainEndpoint . 'country/' . strtolower(str_replace(' ', '_', $country));

        return Api::get($endpoint);
    }

    public static function findAllByCountryAndSeason(string $country, int $seasonAsYear): array
    {
        $endpoint = self::$mainEndpoint . 'country/' . strtolower(str_replace(' ', '_', $country)) . '/' . $seasonAsYear;

        return Api::get($endpoint);
    }

    public static function findAllByCountryAndCurrentSeason(string $country): array
    {
        return self::findAllByCountryAndSeason($country, Helper::getCurrentSeasonAsYear());
    }

    public static function findAllBySeason(int $seasonAsYear): array
    {
        $endpoint = self::$mainEndpoint . 'season/' . $seasonAsYear;

        return Api::get($endpoint);
    }

    public static function findAllByCurrentSeason(): array
    {
        $endpoint = self::$mainEndpoint . 'season/' . Helper::getCurrentSeasonAsYear();

        return Api::get($endpoint);
    }

    public static function findAllByCurrentSeasonAndCountry(string $country): array
    {
        $endpoint = self::$mainEndpoint . 'current/' . strtolower(str_replace(' ', '_', $country));

        return Api::get($endpoint);
    }

    public static function findAllByType(string $type): array
    {
        $endpoint = self::$mainEndpoint . 'type/' . $type;

        return Api::get($endpoint);
    }

    public static function findAllByTypeAndCountry(string $type, string $country): array
    {
        $endpoint = self::$mainEndpoint . 'type/' . $type . '/' . strtolower(str_replace(' ', '_', $country));

        return Api::get($endpoint);
    }

    public static function findAllByTypeAndCountryAndSeason(string $type, string $country, int $seasonAsYear): array
    {
        $endpoint = self::$mainEndpoint . 'type/' . $type . '/' . strtolower(str_replace(' ', '_', $country)) . '/' . $seasonAsYear;

        return Api::get($endpoint);
    }

    public static function findAllByTypeAndCountryAndCurrentSeason(string $type, string $country): array
    {
        $seasonAsYear = Helper::getCurrentSeasonAsYear();

        return self::findAllByTypeAndCountryAndSeason($type, $country, $seasonAsYear);
    }

    public static function findAllByTypeAndSeason(string $type, int $seasonAsYear): array
    {
        $endpoint = self::$mainEndpoint . 'type/' . $type . '/' . $seasonAsYear;

        return Api::get($endpoint);
    }

    public static function findAllByTypeAndCurrentSeason(string $type): array
    {
        $seasonAsYear = Helper::getCurrentSeasonAsYear();

        return self::findAllByTypeAndSeason($type, $seasonAsYear);
    }
}
