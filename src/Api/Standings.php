<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Standings
{
    protected static string $mainEndpoint = 'leagueTable/';

    public static function findByLeagueId(int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . $leagueId;

        return Api::get($endpoint);
    }
}
