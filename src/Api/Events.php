<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Events
{
    protected static string $mainEndpoint = 'events/';

    public static function getEventsOfFixture(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . $fixtureId;

        return Api::get($endpoint);
    }
}
