<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Statistics
{
    protected static string $mainEndpoint = 'statistics/';

    public static function findByTeamIdAndLeagueId(int $teamId, int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . $leagueId . '/' . $teamId;

        return Api::get($endpoint);
    }

    public static function findByTeamIdAndLeagueIdAndDate(int $teamId, int $leagueId, string $date): array
    {
        $endpoint = self::$mainEndpoint . $leagueId . '/' . $teamId . '/' . $date;

        return Api::get($endpoint);
    }

    public static function getStatisticsOfFixture(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . 'fixture/' . $fixtureId;

        return Api::get($endpoint);
    }

}
