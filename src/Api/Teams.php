<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Teams
{
    protected static string $mainEndpoint = 'teams/';

    public static function findByTeamId(int $teamId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId;

        return Api::get($endpoint);
    }

    public static function getRecordByTeamId(int $teamId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId;

        return Api::get($endpoint)['teams'][0];
    }

    public static function getFullRecordByTeamId(int $teamId, int $leagueId): array
    {
        $teamRecord = self::getRecordByTeamId($teamId);
        $teamRecord['leagues'] = Leagues::findAllByTeamIdAndCurrentSeason($teamId);
        $teamRecord['statistics'] = Statistics::findByTeamIdAndLeagueId($teamId, $leagueId);

        return $teamRecord;
    }

    public static function findTeamsByLeague(int $leagueId): array
    {
        $endpoint = self::$mainEndpoint . 'league/' . $leagueId;

        return Api::get($endpoint);
    }

    public static function findTeamsByName(string $name): array
    {
        $endpoint = self::$mainEndpoint . 'search/' . strtolower(str_replace(' ', '_', $name));

        return Api::get($endpoint);
    }

    public static function findTeamsByCountry(string $country): array
    {
        return self::findTeamsByName($country);
    }
}
