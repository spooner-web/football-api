<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class LineUps
{
    protected static string $mainEndpoint = 'lineups/';

    public static function getLineupsOfFixture(int $fixtureId): array
    {
        $endpoint = self::$mainEndpoint . $fixtureId;

        return Api::get($endpoint);
    }
}
