<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Seasons
{
    protected static string $mainEndpoint = 'seasons/';

    public static function all(): array
    {
        $endpoint = self::$mainEndpoint;

        return Api::get($endpoint);
    }

    public static function currentByTeam(int $teamId, int $year = 0): int
    {
        $currentYear = $year ?: Helper::getCurrentSeasonAsYear();
        $endpoint = 'leagues/team/' . $teamId . '/' . $currentYear;
        $return = Api::get($endpoint);
        $results = $return['results'];
        while ($results === 0) {
            --$currentYear;
            $endpoint = 'leagues/team/' . $teamId . '/' . $currentYear;
            $return = Api::get($endpoint);
            $results = $return['results'];
        }

        return (int)$currentYear;
    }
}
