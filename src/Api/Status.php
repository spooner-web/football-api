<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Status
{
    public static function show(): array
    {
        $endpoint = 'status';
        return Api::get($endpoint);
    }
}
