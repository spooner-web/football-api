<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Api;

use SpoonerWeb\Football\Api;

class Coachs
{
    protected static string $mainEndpoint = 'coachs/';

    public static function findOneCoachByCoachId(int $coachId): array
    {
        $endpoint = self::$mainEndpoint . 'coach/' . $coachId;

        return Api::get($endpoint);
    }

    public static function findAllCoachsByTeamId(int $teamId): array
    {
        $endpoint = self::$mainEndpoint . 'team/' . $teamId;

        return Api::get($endpoint);
    }

    public static function findAllCoachsBySearch(string $searchString): array
    {
        $endpoint = self::$mainEndpoint . 'search/' . strtolower(str_replace(' ', '_', $searchString));

        return Api::get($endpoint);
    }
}
