<?php
namespace SpoonerWeb\Football\Api;

class Helper
{
    public static function getCurrentSeasonAsYear(): int
    {
        $seasonAsYear = (int)date('Y');
        if ((int)date('m') <= 6) {
            $seasonAsYear--;
        }

        return $seasonAsYear;
    }
}
