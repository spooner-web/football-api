<?php
declare(strict_types=1);

namespace SpoonerWeb\Football;

class Configuration
{
    public static string $entrypoint = 'https://v2.api-football.com/';

    protected static string $token = '';

    protected static string $timezone = '';

    public static function getToken(): string
    {
        return getenv('FOOTBALL_API_TOKEN') ?: self::$token;
    }

    public static function setToken(string $token): void
    {
        self::$token = $token;
    }

    public static function getTimezone(): string
    {
        $timezone = self::$timezone ?: date_default_timezone_get() ?: 'Europe/London';
        if ($timezone === 'UTC') {
            $timezone = 'Europe/London';
        }

        return $timezone;
    }

    public static function setTimezone(string $timezone): void
    {
        self::$timezone = $timezone;
    }
}
