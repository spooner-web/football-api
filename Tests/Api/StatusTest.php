<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Status;
use SpoonerWeb\Football\Configuration;

class StatusTest extends AbstractApiTests
{
    public function testShowStatusShowsArray(): void
    {
        self::assertIsArray(Status::show());
    }

    public function testShowStatusShowsToken(): void
    {
        $token = getenv('FOOTBALL_API_TOKEN');
        Configuration::setToken($token);
        self::assertEquals(
            'Yes',
            Status::show()['status']['active']
        );
    }
}
