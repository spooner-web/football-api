<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Teams;

class TeamsTest extends AbstractApiTests
{
    public function testShowTeamWithTeamIdShowsTeam(): void
    {
        $teamId = 172;
        $result = Teams::findByTeamId($teamId);
        self::defaultApiResultsTest($result);
        self::assertEquals($teamId, $result['teams'][0]['team_id']);
    }

    public function testShowTeamRecordOnlyWithTeamIdShowsTeamRecord(): void
    {
        $teamId = 172;
        $result = Teams::getRecordByTeamId($teamId);
        self::assertIsArray($result);
        self::assertEquals($teamId, $result['team_id']);
    }

    public function testShowFullTeamRecordOnlyWithTeamIdShowsTeamRecordWithLeagues(): void
    {
        $teamId = 172;
        $leagueId = 755;
        $result = Teams::getFullRecordByTeamId($teamId, $leagueId);
        self::assertIsArray($result);
        self::assertEquals($teamId, $result['team_id']);
        self::assertIsArray($result['leagues']);
        self::assertIsArray($result['statistics']);
    }

    public function testShowAllTeamsWithLeagueIdShowsAllTeamsFromLeague(): void
    {
        $leagueId = 8;
        $result = Teams::findTeamsByLeague($leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testSearchTeamWithNameAsStringShowsTeamsWithName(): void
    {
        $name = 'Liverpool';
        $result = Teams::findTeamsByName($name);
        self::defaultApiResultsTest($result);
    }

    public function testSearchTeamWithCountryAsStringShowsTeamInCountry(): void
    {
        $country = 'England';
        $result = Teams::findTeamsByCountry($country);
        self::defaultApiResultsTest($result);
    }
}
