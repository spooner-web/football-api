<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Players;

class PlayersTest extends AbstractApiTests
{
    public function testGetOneCoachWithCoachIdReturnsCoach(): void
    {
        $result = Players::getAllAvailableSeasonsForPlayerStatistics();
        self::defaultApiResultsTest($result);
    }

    public function testFindAllPlayersWithSearchStringReturnsAllPlayersWithLastName(): void
    {
        $lastName = 'Ronaldo';
        $result = Players::findAllPlayersByLastName($lastName);
        self::defaultApiResultsTest($result);
    }

    public function testGetSquadWithTeamIdAndSeasonAsYearReturnsSquad(): void
    {
        $teamId = 172;
        $season = 2020;
        $result = Players::getSquadByTeamAndSeason($teamId, $season);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllStatisticsWithPlayerIdReturnsArrayWithStatistics(): void
    {
        $playerId = 505;
        $result = Players::getAllStatisticsByPlayer($playerId);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllStatisticsWithPlayerIdAndSeasonAsYearReturnsArrayWithStatistics(): void
    {
        $playerId = 505;
        $season = 2020;
        $result = Players::getAllStatisticsByPlayerAndSeason($playerId, $season);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllStatisticsWithTeamIdAndSeasonAsYearReturnsArrayWithStatistics(): void
    {
        $teamId = 172;
        $season = 2020;
        $result = Players::getAllStatisticsByTeamAndSeason($teamId, $season);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllStatisticsWithFixtureIdReturnsArrayWithStatistics(): void
    {
        $fixtureId = 587184;
        $result = Players::getAllStatisticsByFixtureId($fixtureId);
        self::defaultApiResultsTest($result);
    }
}
