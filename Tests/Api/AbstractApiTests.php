<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use SpoonerWeb\Football\Configuration;

class AbstractApiTests extends TestCase
{
    public function setUp(): void
    {
        Configuration::setToken(getenv('FOOTBALL_API_TOKEN'));
    }

    public static function defaultApiResultsTest(array $result, string $index = '', bool $canBeZeroResults = false): void
    {
        if (!$index) {
            $reflect = new ReflectionClass(static::class);
            $index = lcfirst(str_replace('Test', '', $reflect->getShortName()));
        }
        self::assertIsArray($result);
        if ($canBeZeroResults) {
            self::assertGreaterThanOrEqual(0, $result['results']);
        } else {
            self::assertGreaterThan(0, $result['results']);
        }
        self::assertIsArray($result[$index]);
    }
}
