<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\LineUps;

class LineUpsTest extends AbstractApiTests
{
    public function testGetLineupsWithFixtureIdReturnsLineupsOfFixtureId(): void
    {
        $fixtureId = 587184;
        $result = LineUps::getLineupsOfFixture($fixtureId);
        self::defaultApiResultsTest($result);
    }
}
