<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Events;

class EventsTest extends AbstractApiTests
{
    public function testGetEventsWithFixtureIdReturnsEventsOfFixtureId(): void
    {
        $fixtureId = 587184;
        $result = Events::getEventsOfFixture($fixtureId);
        self::defaultApiResultsTest($result);
    }
}
