<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Statistics;

class StatisticsTest extends AbstractApiTests
{
    public function testGetStatisticsWithTeamIdAndLeagueIdShowsStatistics(): void
    {
        $teamId = 172;
        $leagueId = 755;
        $result = Statistics::findByTeamIdAndLeagueId($teamId, $leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testGetStatisticsWithTeamIdAndLeagueIdAndDateShowsStatistics(): void
    {
        $teamId = 172;
        $leagueId = 755;
        $date = '2019-05-07';
        $result = Statistics::findByTeamIdAndLeagueIdAndDate($teamId, $leagueId, $date);
        self::defaultApiResultsTest($result);
    }

    public function testGetStatisticsWithFixtureIdReturnsStatisticsOfFixtureId(): void
    {
        $fixtureId = 587184;
        $result = Statistics::getStatisticsOfFixture($fixtureId);
        self::defaultApiResultsTest($result);
    }
}
