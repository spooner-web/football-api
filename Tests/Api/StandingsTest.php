<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Standings;

class StandingsTest extends AbstractApiTests
{
    public function testGetLeagueTableByLeagueId(): void
    {
        $leagueId = 2755;
        $result = Standings::findByLeagueId($leagueId);
        self::defaultApiResultsTest($result);
    }
}
