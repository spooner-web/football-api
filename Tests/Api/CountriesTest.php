<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Countries;

class CountriesTest extends AbstractApiTests
{
    public function testShowCountriesShowsAllCountries(): void
    {
        self::assertIsArray(Countries::all());
        self::assertGreaterThan(
            0,
            Countries::all()['results']
        );
        self::assertIsArray(
            Countries::all()['countries']
        );
    }
}
