<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Fixtures;
use SpoonerWeb\Football\Configuration;

class FixturesTest extends AbstractApiTests
{
    public function testGetAllFixturesByLeagueId(): void
    {
        $leagueId = 2755;
        $result = Fixtures::findAllRoundsByLeagueId($leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testGetCurrentFixtureByLeagueId(): void
    {
        $leagueId = 2755;
        $result = Fixtures::findCurrentRoundByLeagueId($leagueId);
        self::assertIsArray($result);
        self::assertGreaterThanOrEqual(0, $result['results']);
    }

    public function testGetOneFixtureByFixtureIdReturnsFixture(): void
    {
        $fixtureId = 587184;
        $result = Fixtures::findOneFixtureById($fixtureId);
        self::assertIsArray($result);
        self::assertEquals($fixtureId, $result['fixture_id']);
    }

    public function testGetLiveFixtures(): void
    {
        $result = Fixtures::findAllLive();
        self::assertIsArray($result);
    }

    public function testGetLiveFixturesByOneLeagueId(): void
    {
        $leagueId = [8];
        $result = Fixtures::findLiveByLeagueIds($leagueId);
        self::assertIsArray($result);
        self::assertGreaterThanOrEqual(0, $result['results']);
    }

    public function testGetLiveFixturesByManyLeagueIds(): void
    {
        $leagueIds = [8, 3, 7];
        $result = Fixtures::findLiveByLeagueIds($leagueIds);
        self::assertIsArray($result);
        self::assertGreaterThanOrEqual(0, $result['results']);
    }

    public function testGetFixturesByDateReturnsArrayWithFixturesFromDate(): void
    {
        $date = new \DateTime('2020-09-19');
        $result = Fixtures::findAllByDate($date);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByDateAndAnotherTimezoneReturnsArrayWithFixturesFromDate(): void
    {
        $date = new \DateTime('2020-09-19');
        $currentTimezone = Configuration::getTimezone();
        $otherTimezone = 'Australia/Sydney';
        Configuration::setTimezone($otherTimezone);
        $result = Fixtures::findAllByDate($date);
        self::defaultApiResultsTest($result);
        Configuration::setTimezone($currentTimezone);
    }

    public function testGetFixturesByLeagueIdReturnsArrayWithFixturesFromLeague(): void
    {
        $leagueId = 8;
        $result = Fixtures::findAllByLeagueId($leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByLeagueIdAndDateReturnsArrayWithFixturesFromLeagueAndDate(): void
    {
        $leagueId = 2755;
        $date = new \DateTime('2020-09-19');
        $result = Fixtures::findAllByLeagueIdAndDate($leagueId, $date);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByLeagueIdWithoutDateReturnsArrayWithFixturesFromLeagueAndDate(): void
    {
        $leagueId = 2755;
        $result = Fixtures::findAllByLeagueIdAndDate($leagueId);
        self::assertIsArray($result);
        self::assertGreaterThanOrEqual(0, $result['results']);
        self::assertIsArray($result['fixtures']);

    }

    public function testGetFixturesByLeagueAndRoundReturnsArrayWithFixturesOfSelectedRound(): void
    {
        $leagueId = 2755;
        $round = Fixtures::findCurrentRoundByLeagueId($leagueId)['fixtures'][0];
        $result = Fixtures::findAllByLeagueIdAndRound($leagueId, $round);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByLeagueAndNoRoundReturnsArrayWithFixturesOfCurrentRound(): void
    {
        $leagueId = 2755;
        $result = Fixtures::findAllByLeagueIdAndRound($leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByLeagueAndNoRoundReturnsSameResultAsWithCurrentRound(): void
    {
        $leagueId = 2755;
        $resultWithoutRound = Fixtures::findAllByLeagueIdAndRound($leagueId);
        $round = Fixtures::findCurrentRoundByLeagueId($leagueId)['fixtures'][0];
        $resultWithRound = Fixtures::findAllByLeagueIdAndRound($leagueId, $round);
        self::assertEquals($resultWithoutRound, $resultWithRound);
    }

    public function testGetNextFixturesByLeagueIdWithNumberReturnsArrayOfNextFixtures(): void
    {
        $leagueId = 2755;
        $number = 3;
        $result = Fixtures::findNextByLeagueId($leagueId, $number);
        self::defaultApiResultsTest($result, '', true);
        self::assertLessThanOrEqual($number, $result['results']);
    }

    public function testGeLastFixturesByLeagueIdWithNumberReturnsArrayOfLastFixtures(): void
    {
        $leagueId = 2755;
        $number = 3;
        $result = Fixtures::findLastByLeagueId($leagueId, $number);
        self::defaultApiResultsTest($result);
        self::assertLessThanOrEqual($number, $result['results']);
    }

    public function testGetFixtuesByTeamIdReturnsArrayOfFixturesByTeamId(): void
    {
        $teamId = 172;
        $result = Fixtures::findAllByTeamId($teamId);
        self::defaultApiResultsTest($result);
    }

    public function testGetFixturesByTeamAndLeagueIdReturnsArrayOfFixturesByTeamIdAndLeagueId(): void
    {
        $teamId = 172;
        $leagueId = 2755;
        $result = Fixtures::findAllByTeamIdAndLeagueId($teamId, $leagueId);
        self::defaultApiResultsTest($result);
    }

    public function testGetNextFixturesByTeamIdWithNumberReturnsNextFixturesByTeamId(): void
    {
        $teamId = 172;
        $number = 3;
        $result = Fixtures::findNextByTeamId($teamId, $number);
        self::defaultApiResultsTest($result, '', true);
    }

    public function testGetLastFixturesByTeamIdWithNumberReturnsLastFixturesByTeamId(): void
    {
        $teamId = 172;
        $number = 3;
        $result = Fixtures::findLastByTeamId($teamId, $number);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllHead2HeadFixturesWithTwoTeamIdsReturnsArrayWithFixturesAndStatistics(): void
    {
        $teamId1 = 172;
        $teamId2 = 163;
        $result = Fixtures::findAllH2hBetweenTwoLeagueIds($teamId1, $teamId2);
        self::defaultApiResultsTest($result);
    }
}
