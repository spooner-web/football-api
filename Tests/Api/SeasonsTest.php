<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Helper;
use SpoonerWeb\Football\Api\Seasons;

class SeasonsTest extends AbstractApiTests
{
    public function testShowSeasonsShowsAllSeasons(): void
    {
        self::assertIsArray(Seasons::all());
        self::assertGreaterThan(
            0,
            Seasons::all()['results']
        );
        self::assertIsArray(
            Seasons::all()['seasons']
        );
    }

    public function testShowCurrentSeasonByTeam(): void
    {
        $currentYear = Helper::getCurrentSeasonAsYear();
        $leagueTeamId = 172;
        self::assertGreaterThanOrEqual($currentYear, Seasons::currentByTeam($leagueTeamId));
    }

    public function testLastSeasonByTeamAndLastYear(): void
    {
        $lastYear = Helper::getCurrentSeasonAsYear() - 1;
        $leagueTeamId = 172;
        self::assertGreaterThanOrEqual($lastYear, Seasons::currentByTeam($leagueTeamId, $lastYear));
    }

    public function testCurrentSeasonWithByTeamAndNextYear(): void
    {
        $currentYear = Helper::getCurrentSeasonAsYear();
        $nextYear = (int)date('Y', strtotime('+1 year'));
        $leagueTeamId = 172;
        self::assertGreaterThanOrEqual($currentYear, Seasons::currentByTeam($leagueTeamId, $nextYear));
    }
}
