<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Helper;
use SpoonerWeb\Football\Api\Leagues;
use SpoonerWeb\Football\Api\Seasons;

class LeaguesTest extends AbstractApiTests
{
    public function testShowLeaguesShowsAllLeagues(): void
    {
        self::assertIsArray(Leagues::all());
        self::assertGreaterThan(
            0,
            Leagues::all()['results']
        );
        self::assertIsArray(
            Leagues::all()['leagues']
        );
    }

    public function testShowLeaguesByTeamIntShowsLeaguesAndResults(): void
    {
        $leagueId = 8;
        $result = Leagues::findOneById($leagueId);
        self::defaultApiResultsTest($result);
        self::assertEquals($leagueId, $result['leagues'][0]['league_id']);
    }

    public function testShowLeaguesByTeamIntAndGetDirectResultShowsSingleLeague(): void
    {
        $leagueId = 8;
        self::assertIsArray(Leagues::findOneById($leagueId, true));
        self::assertEquals($leagueId, Leagues::findOneById($leagueId, true)['league_id']);
    }

    public function testShowLeaguesByTeamAsIntShowsAllLeaguesTeamPlayedOneGame(): void
    {
        $teamId = 172;
        $result = Leagues::findAllByTeamId($teamId);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTeamAndSeason(): void
    {
        $teamId = 172;
        $year = 2018;
        $result = Leagues::findAllByTeamIdAndSeason($teamId, $year);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTeamAndCurrentSeason(): void
    {
        $teamId = 172;
        $result = Leagues::findAllByTeamIdAndCurrentSeason($teamId);
        self::defaultApiResultsTest($result);
        self::assertEquals(
            Seasons::currentByTeam($teamId),
            Leagues::findAllByTeamIdAndCurrentSeason($teamId)['leagues'][0]['season']
        );
    }

    public function testShowLeaguesBySearchWithNameAsString(): void
    {
        $name = 'Bundesliga 1';
        $result = Leagues::findAllBySearchString($name);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesBySearchWithCountryAsString(): void
    {
        $country = 'Germany';
        $result = Leagues::findAllBySearchString($country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByCountryAsString(): void
    {
        $country = 'Germany';
        $result = Leagues::findAllByCountry($country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByCountryAsShortStringCode(): void
    {
        $country = 'DE';
        $result = Leagues::findAllByCountry($country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByCountryAsStringAndSeasonAsYear(): void
    {
        $country = 'Germany';
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByCountryAndSeason($country, $year);
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllByCountryAndSeason($country, $year)['leagues'][0]['season']);
    }

    public function testShowLeaguesByCountryAsStringAndCurrentSeason(): void
    {
        $country = 'Germany';
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByCountryAndCurrentSeason($country);
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllByCountryAndCurrentSeason($country)['leagues'][0]['season']);
    }

    public function testShowLeaguesByCountryAsShortCodeAndSeasonAsYear(): void
    {
        $country = 'DE';
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByCountryAndSeason($country, $year);
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllByCountryAndSeason($country, $year)['leagues'][0]['season']);
    }

    public function testShowLeaguesByCountryAsShortCodeAndCurrentSeason(): void
    {
        $country = 'DE';
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByCountryAndCurrentSeason($country);
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllByCountryAndCurrentSeason($country)['leagues'][0]['season']);
    }

    public function testShowLeaguesBySeasonAsYear(): void
    {
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllBySeason($year);
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllBySeason($year)['leagues'][0]['season']);
    }

    public function testShowLeaguesByCurrentSeason(): void
    {
        $year = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByCurrentSeason();
        self::defaultApiResultsTest($result);
        self::assertEquals($year, Leagues::findAllByCurrentSeason()['leagues'][0]['season']);
    }

    public function testShowLeaguesByCurrentSeasonAndCountry(): void
    {
        $country = 'Germany';
        $result = Leagues::findAllByCurrentSeasonAndCountry($country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByType(): void
    {
        $type = 'cup';
        $result = Leagues::findAllByType($type);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTypeAndCountry(): void
    {
        $type = 'cup';
        $country = 'Germany';
        $result = Leagues::findAllByTypeAndCountry($type, $country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTypeAndCountryAndSeason(): void
    {
        $type = 'cup';
        $country = 'Germany';
        $season = Helper::getCurrentSeasonAsYear();
        $result = Leagues::findAllByTypeAndCountryAndSeason($type, $country, $season);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTypeAndCountryAndCurrentSeason(): void
    {
        $type = 'cup';
        $country = 'Germany';
        $result = Leagues::findAllByTypeAndCountryAndCurrentSeason($type, $country);
        self::defaultApiResultsTest($result);
    }

    public function testShowLeaguesByTypeAndCurrentSeason(): void
    {
        $type = 'cup';
        $result = Leagues::findAllByTypeAndCurrentSeason($type);
        self::defaultApiResultsTest($result);
    }

}
