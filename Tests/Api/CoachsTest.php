<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Coachs;

class CoachsTest extends AbstractApiTests
{
    public function testGetOneCoachWithCoachIdReturnsCoach(): void
    {
        $coachId = 7611;
        $result = Coachs::findOneCoachByCoachId($coachId);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllCoachsWithTeamIdReturnsCoachsInTeam(): void
    {
        $teamId = 172;
        $result = Coachs::findAllCoachsByTeamId($teamId);
        self::defaultApiResultsTest($result);
    }

    public function testGetAllCoachsWithSearchStringReturnsFoundCoachs(): void
    {
        $searchString = 'Zidane';
        $result = Coachs::findAllCoachsBySearch($searchString);
        self::defaultApiResultsTest($result);
    }
}
