<?php
declare(strict_types = 1);

namespace SpoonerWeb\Football\Tests\Api;

use SpoonerWeb\Football\Api\Predictions;

class PredictionsTest extends AbstractApiTests
{
    public function testGetEventsWithFixtureIdReturnsEventsOfFixtureId(): void
    {
        $fixtureId = 587184;
        $result = Predictions::getPredictionsOfFixture($fixtureId);
        self::defaultApiResultsTest($result);
    }
}
