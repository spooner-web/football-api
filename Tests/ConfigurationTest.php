<?php
declare(strict_types=1);

namespace SpoonerWeb\Football\Tests;

use PHPUnit\Framework\TestCase;
use SpoonerWeb\Football\Configuration;

class ConfigurationTest extends TestCase
{
    /**
     * @test
     */
    public function testEntrypointIsDefined(): void
    {
        self::assertEquals(
            'https://v2.api-football.com/',
            Configuration::$entrypoint
        );
    }

    /**
     * @test
     */
    public function defaultTimeZoneIsSetByPhpIni(): void
    {
        $timezone = 'Europe/Berlin';
        Configuration::setTimezone('');
        date_default_timezone_set($timezone);
        self::assertEquals(
            $timezone,
            Configuration::getTimezone()
        );
    }

    /**
     * @test
     */
    public function defaultTimeZoneIsSetByConfiguration(): void
    {
        $timezoneSetByPhpIni = 'Europe/Berlin';
        date_default_timezone_set($timezoneSetByPhpIni);
        $timezoneSetByConfig = 'Asia/Tokyo';
        Configuration::setTimezone($timezoneSetByConfig);
        self::assertEquals(
            $timezoneSetByConfig,
            Configuration::getTimezone()
        );
    }

    /**
     * @test
     */
    public function defaultTimeZoneIsSetToEmptyStringByConfiguration(): void
    {
        $timezoneSetByPhpIni = 'Europe/Berlin';
        date_default_timezone_set($timezoneSetByPhpIni);
        $timezoneSetByConfig = '';
        Configuration::setTimezone($timezoneSetByConfig);
        self::assertEquals(
            $timezoneSetByPhpIni,
            Configuration::getTimezone()
        );
    }

    /**
     * @test
     */
    public function returnValidTimeZoneIfUtcIsGiven(): void
    {
        Configuration::setTimezone('UTC');
        self::assertEquals(
            'Europe/London',
            Configuration::getTimezone()
        );
    }
}
